$(document).ready(function(){
  $('.slider').slick({
  	arrows:true,
  	dots:true,
  	adaptiveHeight:true,
  	slidesToShow:4,
  	slidesToScroll: 1,
  	speed:300,
  	easing:'linear',
  	infinite:false,
  	initialSlide: 6,
  	autoplay: true,
  	autoplaySpeed: 3000,
  	pauseOnFocus: true,
  	pauseOnHover: true,
  	pauseOnDotsHover: true,
  	touchThreshold:10,
  	waitForAnimate: false,
  	responsive:[
  	   {
       breakpoint: 768,
       settings:{
         slidesToShow:2,
          }
  	   }
  	],

  });
 });
 
